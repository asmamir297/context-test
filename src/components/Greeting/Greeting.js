import React, { useReducer } from 'react';
import axios from 'axios';

const INIT_GREETING = {
    greeting: '',
    error: ''
};

const greetingReducer = (greeting, action) => {

    switch (action.type) {

        case 'SUCCESS':
            return {
                greeting: action.payload,
                error: ''
            };

        case 'ERROR':
            return {
                greeting: '',
                error: action.payload
            }

        default:
            return greeting;

    }

}

function Greeting({ url }) {

    const [{ error, greeting }, dispatch] = useReducer(greetingReducer, INIT_GREETING);

    const buttText = error || !greeting ? 'Load Greeting' : 'Ok';

    const fetchGreeting = async () => {

        try {
            const res = await axios({ url });
            dispatch({ type: 'SUCCESS', payload: res.data.greeting });

        } catch (e) {
            dispatch({ type: 'ERROR', payload: "Opss, something went wrong" });
        }

    }

    return (
        <div>

            <button
                disabled={buttText === 'Ok'}
                onClick={fetchGreeting}
            >
                {buttText}
            </button>

            {greeting && <h1> {greeting} </h1>}

            {error && <p role="alert"> {error} </p>}

        </div>
    )
}

export default React.memo(Greeting);
