import React from 'react';
import { render, fireEvent, wait } from '@testing-library/react';
import Greeting from './Greeting';
import { rest } from 'msw';
import { setupServer } from 'msw/node';

const url = "/c643ed32-cdb9-4eb3-8f7e-944e64f2cb93";
const greeting = "Hello Matin"

const server = setupServer(
    rest.get(url, (req, res, ctx) => {
        return res(ctx.json({ greeting }));
    })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test("load greetings without fear", async () => {

    // arange
    const { getByRole, getByText } = render(<Greeting url={url} />);

    // act
    fireEvent.click(getByText("Load Greeting"));

    await wait(() => getByRole('heading'));

    // assert
    getByText(greeting)
    getByText("Ok")
    expect(getByRole('button')).toHaveAttribute('disabled')
    // getByRole("button").toHaveTextContent("Load Greeting");

})

test("handles server error", async () => {

    // arrange
    server.use(
        rest.get(url, (req, res, ctx) => {
            return res(ctx.status(500));
        })
    )

    const { getByRole, getByText } = render(<Greeting url={url} />);

    // act
    fireEvent.click(getByText("Load Greeting"));

    await wait(() => getByRole("alert"));

    // assert
    expect(getByRole('alert')).toHaveTextContent("Opss, something went wrong");
    expect(getByRole("button")).not.toHaveAttribute("disabled");

})