import React, { useState } from 'react'

function HiddenMessage({ children }) {

    const [showMsg, setShowMsg] = useState(false);

    return (
        <div>

            <label htmlFor="toggle-btn"> {showMsg ? "Hide Message" : "Show Message"} </label>
            <input
                type="checkbox"
                checked={showMsg}
                onChange={({ target }) => setShowMsg(target.checked)}
                id="toggle-btn"
            />
            <br />
            {showMsg && children}
        </div>
    )
}

export default HiddenMessage
