import React from 'react';
import { render, fireEvent } from 'custom-render';
import HiddenMessage from './HiddenMessage';

it("Shows children when checkbox is checked", () => {

    // arrange
    const msg = 'Hello';

    const { getByText, queryByText, getByLabelText } = render(<HiddenMessage> {msg} </HiddenMessage>)

    // assert
    // get* functions will return the element or throw an error if it cannot be found
    getByText("Show Message");
    // query* functions will return the element or null if it cannot be found
    expect(queryByText(msg)).toBeNull();

    // act
    fireEvent.click(getByLabelText("Show Message"));

    getByText(msg);

})