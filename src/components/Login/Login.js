import React, { useState } from 'react';
import isEmail from 'validator/lib/isEmail';

function Login() {

    const [loading, setLoading] = useState(false);
    const [error, setError] = useState('');

    const onSubmitHandler = async e => {

        e.preventDefault();

        const { emailInput, passwordInput } = e.target.elements;

        if (!emailInput.value || !passwordInput.value)
            return setError("email/password are required");

        if (!isEmail(emailInput.value))
            return setError("email is not valid");

        setLoading(true);
        setError('');

        try {

            const res = await fetch("https://run.mocky.io/v3/d79b107b-55f3-4f1e-a0dc-a8840e843a7d", {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    email: emailInput.value,
                    password: passwordInput.value
                })
            });

            const result = await res.json();

            setLoading(false);
            localStorage.setItem("token", result.token);

            props.history.push("/");

        } catch (e) {
            setLoading(false);
            setError(e.message);
        }

    }

    return (
        <div>

            {
                loading ?
                    <p> Loading... </p> :
                    <form onSubmit={onSubmitHandler}>

                        <div>
                            <label htmlFor="emailInput"> Email </label>
                            <input required type="email" id="emailInput" />
                        </div>

                        <div>
                            <label htmlFor="passwordInput"> Password </label>
                            <input required type="password" id="passwordInput" />
                        </div>

                        <button type="submit"> Submit </button>

                    </form>
            }

            {error && <p role="alert"> {error} </p>}

        </div>
    )
}

export default Login;
