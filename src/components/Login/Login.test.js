import React from 'react';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import { render, fireEvent, wait, waitForElementToBeRemoved, waitForElement } from 'custom-render';
import Login from './Login';

const server = setupServer(
    rest.post("https://run.mocky.io/v3/d79b107b-55f3-4f1e-a0dc-a8840e843a7d", (req, res, ctx) => {
        return res(ctx.json({ token: "user_tokenn" }));
    })
)

beforeAll(() => server.listen());
afterEach(() => {
    server.resetHandlers();
    localStorage.removeItem('token');
});
afterAll(() => server.close());

const setup = () => {
    const utils = render(<Login />)

    const emailInput = utils.getByLabelText("Email");
    const passwordInput = utils.getByLabelText("Password");
    const submitButton = utils.getByText("Submit");
    const loadingNode = utils.queryByText(/loading/i);

    return {
        emailInput,
        passwordInput,
        submitButton,
        loadingNode,
        ...utils,
    }
}


it("lets the user to login successfuly", async () => {

    const { emailInput, passwordInput, submitButton, getByText, queryByRole } = setup();

    fireEvent.change(emailInput, { target: { value: "a@a.com" } });
    fireEvent.change(passwordInput, { target: { value: 'abcd1234' } });

    fireEvent.click(submitButton);

    getByText(/loading/i);

    await waitForElementToBeRemoved(() => getByText(/loading/i));

    expect(queryByRole("alert")).toBeNull();
    expect(localStorage.getItem('token')).toEqual("user_tokenn");

    // await waitForElement(() => getByText("Load Greeting"));

});

it("validates empty email and password", () => {

    const { emailInput, passwordInput, submitButton, loadingNode, getByRole } = setup();

    expect(emailInput).toBeEmpty();
    expect(passwordInput).toBeEmpty();

    fireEvent.click(submitButton);

    expect(loadingNode).toBeNull();
    expect(getByRole("alert")).toHaveTextContent("email/password are required");

});

it("validates empty email", () => {

    const { emailInput, passwordInput, submitButton, loadingNode, getByRole } = setup();

    expect(emailInput).toBeEmpty();

    fireEvent.change(passwordInput, { target: { value: 'abcd1234' } });

    fireEvent.click(submitButton);

    expect(loadingNode).toBeNull();
    expect(getByRole("alert")).toHaveTextContent("email/password are required");

});

it("validates empty password", () => {

    const { emailInput, passwordInput, submitButton, loadingNode, getByRole } = setup();

    expect(passwordInput).toBeEmpty();

    fireEvent.change(emailInput, { target: { value: 'a@a.com' } });

    fireEvent.click(submitButton);

    expect(loadingNode).toBeNull();
    expect(getByRole("alert")).toHaveTextContent("email/password are required");

});

it("does not allow unvalid email to login", () => {

    const { emailInput, passwordInput, submitButton, loadingNode, getByRole } = setup();

    fireEvent.change(emailInput, { target: { value: 'a' } });
    fireEvent.change(passwordInput, { target: { value: 'asas' } });

    fireEvent.click(submitButton);

    expect(loadingNode).toBeNull();
    expect(getByRole("alert")).toHaveTextContent("email is not valid");

});

it("Matches the snapshot of the login element", () => {

    const { container } = setup();

    expect(container).toMatchSnapshot();

})