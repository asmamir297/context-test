import React, { useState } from 'react'

function AddTaskForm({ addTaskHandler, text }) {

    const [task, setTask] = useState("");

    return (
        <form onSubmit={addTaskHandler(task)}>

            <input
                placeholder="todo"
                type="text"
                value={task}
                onChange={({ target }) => setTask(target.value)}
            />

            <button type="submit"> {text} </button>

        </form>
    )
}

export default React.memo(AddTaskForm)
