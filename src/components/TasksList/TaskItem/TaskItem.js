import React from 'react'

function TaskItem({ title, id, deleteTaskHandler }) {
    return (
        <div className="task">
            {title}
            <img
                src="https://cdn2.iconfinder.com/data/icons/medical-and-health-2-16/65/64-512.png"
                alt="delete icon"
                onClick={deleteTaskHandler(id)}
            />
        </div>
    )
}

export default React.memo(TaskItem);
