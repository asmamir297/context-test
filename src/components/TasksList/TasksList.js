import React, { useCallback } from 'react';
import clsx from 'clsx';
import { lang } from '../../constants';
import { useApplicationState } from '../../store/context';
import { ADD_TASK, DELETE_TASK } from '../../store/tasks';
import AddTaskForm from './AddTaskForm/AddTaskForm';
import TaskItem from './TaskItem/TaskItem';
import './TasksList.scss';

function App() {

  const [{ tasks, theme }, dispatch] = useApplicationState();

  const addTaskHandler = useCallback(taskTitle => e => {
    e.preventDefault();
    dispatch({ type: ADD_TASK, payload: taskTitle });
  }, [dispatch])

  const deleteTaskHandler = useCallback(id => () => {
    dispatch({ type: DELETE_TASK, payload: id });
  }, [dispatch])


  return (
    <div className={
      clsx(
        "App",
        theme.theme === 'dark' && "App--dark",
        theme.theme === 'light' && "App--light",
      )
    }>

      <AddTaskForm
        text={lang[theme.lang].add}
        addTaskHandler={addTaskHandler}
      />

      <div data-testid="todo-items">
        {
          Object.values(tasks)
            .map(task => (
              <TaskItem
                key={task.id}
                {...task}
                deleteTaskHandler={deleteTaskHandler}
              />
            ))
        }
      </div>

    </div>
  );
}

export default App;
