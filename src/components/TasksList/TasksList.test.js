import React from 'react';
import { render, fireEvent, wait } from 'custom-render';
import TasksList from './TasksList';

test("list of todos is empty on mount", () => {

    // arrange
    const { getByTestId } = render(<TasksList />);

    expect(getByTestId("todo-items")).toBeEmpty();

})

// test("fetching list of todos from server", async () => {

//     // arrange
//     const { getByTestId, getByText } = render(<TasksList />);

//     await wait(() => getByText("task 1"));

//     expect(getByTestId("todo-items")).not.toBeEmpty();

// })

test("adding todo items", () => {

    // arrange
    const { getByText, getByPlaceholderText } = render(<TasksList />);

    const input = getByPlaceholderText("todo");
    const button = getByText("Add");

    // act
    fireEvent.change(input, { target: { value: 'my todo 1' } });
    fireEvent.click(button);

    fireEvent.change(input, { target: { value: 'my todo 2' } });
    fireEvent.click(button);

    // assert
    getByText('my todo 1');
    getByText('my todo 2');
    // getByText('my todo 3');

})