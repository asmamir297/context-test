export const lang = {
    'fa': {
        langButt: 'انگلیسی',
        lightTheme: 'تاریک',
        darkTheme: 'روشن',
        add: 'اضافه کن'
    },
    'eng': {
        langButt: 'Persian',
        lightTheme: 'Dark',
        darkTheme: 'Light',
        add: 'Add'
    }
}