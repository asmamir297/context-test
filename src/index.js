import React from 'react';
import ReactDOM from 'react-dom';
import { AppProvider } from './store/context';
import rootReducer, { INIT_STATE } from './store/root';
import Router from './router/Router';
import './index.scss';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <React.StrictMode>
    <AppProvider
      reducer={rootReducer}
      initialState={INIT_STATE}
    >
      <Router />
    </AppProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
