import React, { useEffect } from 'react';
import { BrowserRouter, Route, Switch, NavLink } from 'react-router-dom';
import { lang } from '../constants';
import { useApplicationState } from '../store/context';
import { getTasksAPI } from '../store/tasks';
import { TOGGLE_LANG, TOGGLE_THEME } from '../store/theme';
import TasksList from '../components/TasksList/TasksList';
import Greeting from '../components/Greeting/Greeting';
import HiddenMessage from '../components/HiddenMessage/HiddenMessage';
import Login from '../components/Login/Login';

function Router() {

    const [{ theme }, dispatch] = useApplicationState();

    const toggleThemeHandler = () => {
        dispatch({ type: TOGGLE_THEME });
    }

    const toggleLangHandler = () => {
        dispatch({ type: TOGGLE_LANG });
    }

    useEffect(() => {

        getTasksAPI(dispatch);

    }, [dispatch])

    return (
        <BrowserRouter>

            <header>

                <button onClick={toggleThemeHandler}>
                    {
                        theme.theme === 'dark' ?
                            lang[theme.lang].darkTheme :
                            lang[theme.lang].lightTheme
                    }
                </button>

                <nav>
                    <NavLink to="/tasks"> Tasks </NavLink>
                    <NavLink to="/"> Greeting </NavLink>
                </nav>

                <button onClick={toggleLangHandler}>
                    {lang[theme.lang].langButt}
                </button>

            </header>

            <Switch>

                <Route exact path="/" render={() => <Greeting url="/c643ed32-cdb9-4eb3-8f7e-944e64f2cb93" />} />

                <Route path="/login" component={Login} />

                <Route path="/hidden-msg" render={() => <HiddenMessage> You're in the hidden-msg route </HiddenMessage>} />

                <Route path="/tasks" component={TasksList} />

            </Switch>

        </BrowserRouter>
    )
}

export default Router
