import React, { createContext, useContext, useReducer } from 'react';

// application context
export const AppContext = createContext();

// application provider component
export const AppProvider = ({ children, reducer, initialState }) => (
    <AppContext.Provider value={useReducer(reducer, initialState)}>
        {children}
    </AppContext.Provider>
)

// get application state
// custom hook
export const useApplicationState = () => useContext(AppContext);