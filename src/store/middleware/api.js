import axios from 'axios';

axios.defaults.baseURL = 'https://run.mocky.io/v3';

const apiMW = async action => {

    const { method, url, onSuccess, onFailure } = action.payload;

    const res = await axios({
        url,
        method
    })

    if (res.status === 200)
        onSuccess(res.data);

    else onFailure();

}

export default apiMW;