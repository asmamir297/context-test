import themeReducer from './theme';
import tasksReducer from './tasks';
import { CALL_API } from './api';
import apiMW from './middleware/api';

export const INIT_STATE = {
    tasks: {},
    theme: {
        lang: 'eng',
        theme: 'dark'
    }
}

const rootReducer = ({ tasks, theme }, action) => {

    if (action.type === CALL_API)
        apiMW(action);

    return {
        tasks: tasksReducer(tasks, action),
        theme: themeReducer(theme, action)
    }

};

export default rootReducer;