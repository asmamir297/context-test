import { v4 } from 'uuid';
import { CALL_API } from './api';

// actions
export const ADD_TASK = 'ADD_TASK';
export const DELETE_TASK = 'DELETE_TASK';
const GET_TASKS = 'GET_TASKS';

// reducer
const tasksReducer = (tasks, action) => {

    switch (action.type) {

        case GET_TASKS:
            return {
                ...action.payload
            }

        case ADD_TASK:
            const id = v4();
            return {
                ...tasks,
                [id]: {
                    id,
                    title: action.payload
                }
            };

        case DELETE_TASK:
            const newTasks = { ...tasks };
            delete newTasks[action.payload];
            return newTasks;

        default:
            return tasks
    }

}

export default tasksReducer;

// api
// NOTE: there is no way to access dispatch outta react component
export const getTasksAPI = (dispatch) => {

    dispatch({
        type: CALL_API,
        payload: {
            url: '/24318533-e61c-4110-9686-2db5c736f6d3',
            method: 'get',
            onSuccess: data => dispatch({ type: GET_TASKS, payload: data.tasks })
        }
    })

}