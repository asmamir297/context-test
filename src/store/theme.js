// actions
export const TOGGLE_THEME = 'TOGGLE_THEME';
export const TOGGLE_LANG = 'TOGGLE_LANG';

// reducer
const themeReducer = (theme, action) => {

    switch (action.type) {

        case TOGGLE_THEME:
            return {
                ...theme,
                theme: theme.theme === 'dark' ? 'light' : 'dark'
            }

        case TOGGLE_LANG:
            return {
                ...theme,
                lang: theme.lang === 'eng' ? 'fa' : 'eng'
            }

        default:
            return theme;

    }

}

export default themeReducer;