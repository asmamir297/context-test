import React from 'react';
import { render } from '@testing-library/react';
import { AppProvider } from '../store/context';
import rootReducer, { INIT_STATE } from '../store/root';


const AllProviders = ({ children }) => (
    <AppProvider
        reducer={rootReducer}
        initialState={INIT_STATE}
    >
        {children}
    </AppProvider>
);

const customRender = (ui, options) =>
    render(ui, { wrapper: AllProviders, ...options });

// re-export everything
export * from '@testing-library/react';

// override render method
export { customRender as render };
